# Lab5 -- Integration testing

## InnoCar Specs:


- Budet car price per minute = 17
- Luxury car price per minute = 38
- Fixed price per km = 14
- Allowed deviations in % = 10
- Inno discount in % = 6

## BVA
|CONDITIONS|VALUES            |R1    |R2    |R3         |R4    |R5    |R6         |R7(any other)  |
|----------|------------------|------|------|-----------|------|------|-----------|---------------|
|TYPE|budget, luxury, nonsense|budget|luxury|budget     |budget|luxury|budget     |*              |
|PLAN|minute, fixed_price     |minute|minute|fixed_price|minute|minute|fixed_price|*              |
|DIST|>0, <=0, nonsense       |>0    |>0    |>0         |>0    |>0    |>0         |*              |
|PLAN DIST|>0, <=0, nonsense  |>0    |>0    |>0         |>0    |>0    |>0         |*              |
|TIME|>0, <=0, nonsense       |>0    |>0    |>0         |>0    |>0    |>0         |*              |
|PLAN TIME|>0, <=0, nonsense  |>0    |>0    |>0         |>0    |>0    |>0         |*              |
|DISC|no, yes, nonsense       |yes   |yes   |yes        |no    |no    |no         |*              |
|STATUS|OK, Invalid Request   |OK    |OK    |OK         |OK    |OK    |OK         |Invalid Request|

## Domain Tests

|№|TYPE|PLAN|DIST|PLAN DIST|TIME|PLAN TIME|DISC|EXPECTED|RECEIVED|STATUS|
|---|------|-----------|---|---|---|---|---|------------------|------------------|---------------|
|1  |luxury|fixed_price|10 |10 |10 |10 |no |Invalid request   |Invalid request   |OK             |
|2  |budget|minute     |-10|10 |10 |10 |no |Invalid request   |170   |DOMAIN             |
|3 |budget|minute     |10 |-10|10 |10 |no |Invalid request   |170   |DOMAIN             |
|4 |budget|minute     |10 |10 |-10|10 |no |Invalid request   |Invalid request   |OK             |
|5 |budget|minute     |10 |10 |10 |-10|no |Invalid request   |170   |DOMAIN             |
|6 |nonsense|minute     |10 |10 |10 |10 |no |Invalid request   |Invalid request   |OK             |
|7 |budget|nonsense     |10 |10 |10 |10 |no |Invalid request   |Invalid request   |OK             |
|8 |budget|minute     |nonsense|10 |10 |10 |no |Invalid request   |170   |DOMAIN             |
|9 |budget|minute     |10 |nonsense|10 |10 |no |Invalid request   |170   |DOMAIN             |
|10 |budget|minute     |10 |10 |nonsense|10 |no |Invalid request   |null   |DOMAIN             |
|11 |budget|minute     |10 |10 |10 |nonsense|no |Invalid request   |170   |DOMAIN             |
|12 |budget|minute     |10 |10 |10 |10 |nonsense|Invalid request   |Invalid request   |OK             |


## Computational Tests

|№|TYPE|PLAN|DIST|PLAN DIST|TIME|PLAN TIME|DISC|EXPECTED|RECEIVED|STATUS|
|---|------|-----------|---|---|---|---|---|------------------|------------------|---------------|
|1  |budget|fixed_price|10 |10 |10 |10 |yes|131.6             |125               |BAD_CALC       |
|2  |budget|fixed_price|10 |10 |10 |10 |no |140               |125               |BAD_CALC       |
|3  |budget|fixed_price|10 |10 |10 |20 |yes|131.6             |166.66666666666666|BAD_CALC       |
|4  |budget|fixed_price|10 |10 |10 |20 |no |140               |166.66666666666666|BAD_CALC       |
|5  |budget|fixed_price|10 |10 |20 |10 |yes|319.59999999999997|333.3333333333333 |BAD_CALC       |
|6  |budget|fixed_price|10 |10 |20 |10 |no |340               |333.3333333333333 |BAD_CALC       |
|7  |budget|fixed_price|10 |10 |20 |20 |yes|131.6             |125               |BAD_CALC       |
|8  |budget|fixed_price|10 |10 |20 |20 |no |140               |125               |BAD_CALC       |
|9  |budget|fixed_price|10 |20 |10 |10 |yes|263.2             |166.66666666666666|BAD_CALC       |
|10 |budget|fixed_price|10 |20 |10 |10 |no |280               |166.66666666666666|BAD_CALC       |
|11 |budget|fixed_price|10 |20 |10 |20 |yes|263.2             |166.66666666666666|BAD_CALC       |
|12 |budget|fixed_price|10 |20 |10 |20 |no |280               |166.66666666666666|BAD_CALC       |
|13 |budget|fixed_price|10 |20 |20 |10 |yes|319.59999999999997|333.3333333333333 |BAD_CALC       |
|14 |budget|fixed_price|10 |20 |20 |10 |no |340               |333.3333333333333 |BAD_CALC       |
|15 |budget|fixed_price|10 |20 |20 |20 |yes|263.2             |333.3333333333333 |BAD_CALC       |
|16 |budget|fixed_price|10 |20 |20 |20 |no |280               |333.3333333333333 |BAD_CALC       |
|17 |budget|fixed_price|20 |10 |10 |10 |yes|159.79999999999998|166.66666666666666|BAD_CALC       |
|18 |budget|fixed_price|20 |10 |10 |10 |no |170               |166.66666666666666|BAD_CALC       |
|19 |budget|fixed_price|20 |10 |10 |20 |yes|159.79999999999998|166.66666666666666|BAD_CALC       |
|20 |budget|fixed_price|20 |10 |10 |20 |no |170               |166.66666666666666|BAD_CALC       |
|21 |budget|fixed_price|20 |10 |20 |10 |yes|319.59999999999997|333.3333333333333 |BAD_CALC       |
|22 |budget|fixed_price|20 |10 |20 |10 |no |340               |333.3333333333333 |BAD_CALC       |
|23 |budget|fixed_price|20 |10 |20 |20 |yes|319.59999999999997|333.3333333333333 |BAD_CALC       |
|24 |budget|fixed_price|20 |10 |20 |20 |no |340               |333.3333333333333 |BAD_CALC       |
|25 |budget|fixed_price|20 |20 |10 |10 |yes|263.2             |250               |BAD_CALC       |
|26 |budget|fixed_price|20 |20 |10 |10 |no |280               |250               |BAD_CALC       |
|27 |budget|fixed_price|20 |20 |10 |20 |yes|263.2             |166.66666666666666|BAD_CALC       |
|28 |budget|fixed_price|20 |20 |10 |20 |no |280               |166.66666666666666|BAD_CALC       |
|29 |budget|fixed_price|20 |20 |20 |10 |yes|319.59999999999997|333.3333333333333 |BAD_CALC       |
|30 |budget|fixed_price|20 |20 |20 |10 |no |340               |333.3333333333333 |BAD_CALC       |
|31 |budget|fixed_price|20 |20 |20 |20 |yes|263.2             |250               |BAD_CALC       |
|32 |budget|fixed_price|20 |20 |20 |20 |no |280               |250               |BAD_CALC       |
|33 |budget|minute     |10 |10 |10 |10 |yes|159.79999999999998|170               |BAD_CALC       |
|34 |budget|minute     |10 |10 |10 |10 |no |170               |170               |OK             |
|35 |budget|minute     |10 |10 |10 |20 |yes|159.79999999999998|170               |BAD_CALC       |
|36 |budget|minute     |10 |10 |10 |20 |no |170               |170               |OK             |
|37 |budget|minute     |10 |10 |20 |10 |yes|319.59999999999997|340               |BAD_CALC       |
|38 |budget|minute     |10 |10 |20 |10 |no |340               |340               |OK             |
|39 |budget|minute     |10 |10 |20 |20 |yes|319.59999999999997|340               |BAD_CALC       |
|40 |budget|minute     |10 |10 |20 |20 |no |340               |340               |OK             |
|41 |budget|minute     |10 |20 |10 |10 |yes|159.79999999999998|170               |BAD_CALC       |
|42 |budget|minute     |10 |20 |10 |10 |no |170               |170               |OK             |
|43 |budget|minute     |10 |20 |10 |20 |yes|159.79999999999998|170               |BAD_CALC       |
|44 |budget|minute     |10 |20 |10 |20 |no |170               |170               |OK             |
|45 |budget|minute     |10 |20 |20 |10 |yes|319.59999999999997|340               |BAD_CALC       |
|46 |budget|minute     |10 |20 |20 |10 |no |340               |340               |OK             |
|47 |budget|minute     |10 |20 |20 |20 |yes|319.59999999999997|340               |BAD_CALC       |
|48 |budget|minute     |10 |20 |20 |20 |no |340               |340               |OK             |
|49 |budget|minute     |20 |10 |10 |10 |yes|159.79999999999998|170               |BAD_CALC       |
|50 |budget|minute     |20 |10 |10 |10 |no |170               |170               |OK             |
|51 |budget|minute     |20 |10 |10 |20 |yes|159.79999999999998|170               |BAD_CALC       |
|52 |budget|minute     |20 |10 |10 |20 |no |170               |170               |OK             |
|53 |budget|minute     |20 |10 |20 |10 |yes|319.59999999999997|340               |BAD_CALC       |
|54 |budget|minute     |20 |10 |20 |10 |no |340               |340               |OK             |
|55 |budget|minute     |20 |10 |20 |20 |yes|319.59999999999997|340               |BAD_CALC       |
|56 |budget|minute     |20 |10 |20 |20 |no |340               |340               |OK             |
|57 |budget|minute     |20 |20 |10 |10 |yes|159.79999999999998|170               |BAD_CALC       |
|58 |budget|minute     |20 |20 |10 |10 |no |170               |170               |OK             |
|59 |budget|minute     |20 |20 |10 |20 |yes|159.79999999999998|170               |BAD_CALC       |
|60 |budget|minute     |20 |20 |10 |20 |no |170               |170               |OK             |
|61 |budget|minute     |20 |20 |20 |10 |yes|319.59999999999997|340               |BAD_CALC       |
|62 |budget|minute     |20 |20 |20 |10 |no |340               |340               |OK             |
|63 |budget|minute     |20 |20 |20 |20 |yes|319.59999999999997|340               |BAD_CALC       |
|64 |budget|minute     |20 |20 |20 |20 |no |340               |340               |OK             |
|65 |luxury|minute     |10 |10 |10 |10 |yes|357.2             |380               |BAD_CALC       |
|66 |luxury|minute     |10 |10 |10 |10 |no |380               |380               |OK             |
|67 |luxury|minute     |10 |10 |10 |20 |yes|357.2             |380               |BAD_CALC       |
|68 |luxury|minute     |10 |10 |10 |20 |no |380               |380               |OK             |
|69 |luxury|minute     |10 |10 |20 |10 |yes|714.4             |760               |BAD_CALC       |
|70 |luxury|minute     |10 |10 |20 |10 |no |760               |760               |OK             |
|71 |luxury|minute     |10 |10 |20 |20 |yes|714.4             |760               |BAD_CALC       |
|72 |luxury|minute     |10 |10 |20 |20 |no |760               |760               |OK             |
|73 |luxury|minute     |10 |20 |10 |10 |yes|357.2             |380               |BAD_CALC       |
|74 |luxury|minute     |10 |20 |10 |10 |no |380               |380               |OK             |
|75 |luxury|minute     |10 |20 |10 |20 |yes|357.2             |380               |BAD_CALC       |
|76 |luxury|minute     |10 |20 |10 |20 |no |380               |380               |OK             |
|77 |luxury|minute     |10 |20 |20 |10 |yes|714.4             |760               |BAD_CALC       |
|78 |luxury|minute     |10 |20 |20 |10 |no |760               |760               |OK             |
|79 |luxury|minute     |10 |20 |20 |20 |yes|714.4             |760               |BAD_CALC       |
|80 |luxury|minute     |10 |20 |20 |20 |no |760               |760               |OK             |
|81 |luxury|minute     |20 |10 |10 |10 |yes|357.2             |380               |BAD_CALC       |
|82 |luxury|minute     |20 |10 |10 |10 |no |380               |380               |OK             |
|83 |luxury|minute     |20 |10 |10 |20 |yes|357.2             |380               |BAD_CALC       |
|84 |luxury|minute     |20 |10 |10 |20 |no |380               |380               |OK             |
|85 |luxury|minute     |20 |10 |20 |10 |yes|714.4             |760               |BAD_CALC       |
|86 |luxury|minute     |20 |10 |20 |10 |no |760               |760               |OK             |
|87 |luxury|minute     |20 |10 |20 |20 |yes|714.4             |760               |BAD_CALC       |
|88 |luxury|minute     |20 |10 |20 |20 |no |760               |760               |OK             |
|89 |luxury|minute     |20 |20 |10 |10 |yes|357.2             |380               |BAD_CALC       |
|90 |luxury|minute     |20 |20 |10 |10 |no |380               |380               |OK             |
|91 |luxury|minute     |20 |20 |10 |20 |yes|357.2             |380               |BAD_CALC       |
|92 |luxury|minute     |20 |20 |10 |20 |no |380               |380               |OK             |
|93 |luxury|minute     |20 |20 |20 |10 |yes|714.4             |760               |BAD_CALC       |
|94 |luxury|minute     |20 |20 |20 |10 |no |760               |760               |OK             |
|95 |luxury|minute     |20 |20 |20 |20 |yes|714.4             |760               |BAD_CALC       |
|96 |luxury|minute     |20 |20 |20 |20 |no |760               |760               |OK             |

## Summary
**The error domain is listed in the test domain table. Calculation errors occur at a fixed price and in the presence of a discount.**




